module.exports = {
  pathPrefix: "/myplayground",
  plugins: [
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-theme-ui",
    "gatsby-plugin-glslify",
    "gatsby-plugin-transition-link",
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Alfa Slab One`, `Libre Baskerville`, `Quattrocento Sans`],
        display: "swap",
      },
    },
  ],
}
