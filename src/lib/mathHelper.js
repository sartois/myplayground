/**
 * @see https://processing.org/reference/constrain_.html
 *
 * @param {*} n
 * @param {*} low
 * @param {*} high
 * @returns {int}
 */
export function constrain(n, low, high) {
  return Math.round(Math.max(Math.min(n, high), low))
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {Number}
 */
export function getRandomRange(min, max) {
  return Math.random() * (max - min) + min
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 *
 * Using Math.round() will give you a non-uniform distribution!
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 * @returns {int}
 */
export function getRandomRangeInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

/**
 * @see https://github.com/processing/p5.js/blob/1.1.9/src/math/calculation.js#L408
 *
 * @param  {Number} value  the incoming value to be converted
 * @param  {Number} start1 lower bound of the value's current range
 * @param  {Number} stop1  upper bound of the value's current range
 * @param  {Number} start2 lower bound of the value's target range
 * @param  {Number} stop2  upper bound of the value's target range
 * @param  {Boolean} [withinBounds] constrain the value to the newly mapped range
 * @return {Number}
 */
export function map(value, start1, stop1, start2, stop2, withinBounds) {
  const newval =
    ((value - start1) / (stop1 - start1)) * (stop2 - start2) + start2
  if (!withinBounds) {
    return newval
  }
  return start2 < stop2
    ? constrain(newval, start2, stop2)
    : constrain(newval, stop2, start2)
}
