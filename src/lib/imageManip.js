import { Vector2 } from "three"

export function getImageData({ img, size }) {
  return size.height > size.width
    ? getImageDataForPortrait({ img, size })
    : getImageDataForPaysage({ img, size })
}

function getImageDataForPortrait({ img, size }) {
  const canvas = document.createElement("canvas")
  const canvasHeight = img.height
  const canvasWidth = Math.floor((canvasHeight * size.width) / size.height)

  canvas.width = canvasWidth
  canvas.height = canvasHeight

  const ctx = canvas.getContext("2d")

  ctx.drawImage(img, -(img.width - canvasWidth), 0)

  return {
    imgData: ctx.getImageData(0, 0, canvas.width, canvas.height),
    textureSize: new Vector2(canvas.width, canvas.height),
  }
}

function getImageDataForPaysage({ img, size }) {
  const canvas = document.createElement("canvas")
  const canvasWidth = img.width
  const canvasHeight = Math.floor((canvasWidth * size.height) / size.width)

  canvas.width = canvasWidth
  canvas.height = canvasHeight

  const ctx = canvas.getContext("2d")

  ctx.drawImage(img, 0, -(img.height - canvasHeight))

  return {
    imgData: ctx.getImageData(0, 0, canvas.width, canvas.height),
    textureSize: new Vector2(canvas.width, canvas.height),
  }
}
