import { RawShaderMaterial, Vector2 } from "three"
import { extend } from "react-three-fiber"
import vertexShader from "./particle.vert"
import fragmentShader from "./particle.frag"

class ImageParticuleMaterial extends RawShaderMaterial {
  constructor() {
    super({
      vertexShader,
      fragmentShader,
      uniforms: {
        uTime: { value: 0 },
        uRandom: { value: 1.0 },
        uDepth: { value: 2.0 },
        uSize: { value: 0.0 },
        uTextureSize: { value: new Vector2() },
        uTexture: { value: null },
        uTouch: { value: null },
      },
      depthTest: false,
      transparent: true,
    })
  }

  set time(value) {
    this.uniforms.uTime.value = value
  }

  get time() {
    return this.uniforms.uTime.value
  }

  set random(value) {
    this.uniforms.uRandom.value = value
  }

  get random() {
    return this.uniforms.uRandom.value
  }

  set depth(value) {
    this.uniforms.uDepth.value = value
  }

  get depth() {
    return this.uniforms.uDepth.value
  }

  set size(value) {
    this.uniforms.uSize.value = value
  }

  get size() {
    return this.uniforms.uSize.value
  }

  set textureSize(value) {
    this.uniforms.uTextureSize.value = value
  }

  get textureSize() {
    return this.uniforms.uTextureSize.value
  }

  set texture(value) {
    this.uniforms.uTexture.value = value
  }

  get texture() {
    return this.uniforms.uTexture.value
  }

  set touch(value) {
    this.uniforms.uTouch.value = value
  }

  get touch() {
    return this.uniforms.uTouch.value
  }
}

extend({ ImageParticuleMaterial })
