import {
  InstancedBufferGeometry,
  BufferAttribute,
  InstancedBufferAttribute,
  LinearFilter,
  RGBFormat,
} from "three"
import { getImageData } from "../imageManip"

export default function getImageParticuleGeometry(texture, size) {
  texture.minFilter = LinearFilter
  texture.magFilter = LinearFilter
  texture.format = RGBFormat
  const img = texture.image

  const { imgData, textureSize } = getImageData({ img, size })

  const numPoints = textureSize.width * textureSize.height

  let numVisible = 0
  let threshold = 15
  let originalColors

  // console.log("imgdata", numPoints, imgData.data.length / 4);
  originalColors = Float32Array.from(imgData.data)

  for (let i = 0; i < numPoints; i++) {
    if (originalColors[i * 4 + 0] > threshold) numVisible++
  }

  const geometry = new InstancedBufferGeometry()

  // positions
  const positions = new BufferAttribute(new Float32Array(4 * 3), 3)
  positions.setXYZ(0, -0.5, 0.5, 0.0)
  positions.setXYZ(1, 0.5, 0.5, 0.0)
  positions.setXYZ(2, -0.5, -0.5, 0.0)
  positions.setXYZ(3, 0.5, -0.5, 0.0)
  geometry.setAttribute("position", positions)

  // uvs
  const uvs = new BufferAttribute(new Float32Array(4 * 2), 2)
  uvs.setXYZ(0, 0.0, 0.0)
  uvs.setXYZ(1, 1.0, 0.0)
  uvs.setXYZ(2, 0.0, 1.0)
  uvs.setXYZ(3, 1.0, 1.0)
  geometry.setAttribute("uv", uvs)

  // index
  geometry.setIndex(new BufferAttribute(new Uint16Array([0, 2, 1, 2, 3, 1]), 1))

  const indices = new Uint16Array(numVisible)
  //aka position for each instanced geometry
  const offsets = new Float32Array(numVisible * 3)
  const angles = new Float32Array(numVisible)

  for (let i = 0, j = 0; i < numPoints; i++) {
    if (originalColors[i * 4 + 0] <= threshold) continue

    offsets[j * 3 + 0] = i % textureSize.width
    offsets[j * 3 + 1] = Math.floor(i / textureSize.width)

    indices[j] = i

    angles[j] = Math.random() * Math.PI

    j++
  }

  //instanced geometry index
  geometry.setAttribute(
    "pindex",
    new InstancedBufferAttribute(indices, 1, false)
  )
  //instanced geometry position
  geometry.setAttribute(
    "offset",
    new InstancedBufferAttribute(offsets, 3, false)
  )
  //random angle for animation
  geometry.setAttribute("angle", new InstancedBufferAttribute(angles, 1, false))

  return [geometry, textureSize]
}
