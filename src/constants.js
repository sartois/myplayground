export const EXAMPLE_PATH = "blog-starter";
export const CMS_NAME = "Markdown";
export const HOME_OG_IMAGE_URL = "";
export const BLOG_HTML_TITLE = "My Playground - Sylvain Artois";
export const BLOG_TITLE = "My Playground";
export const MENU_LINKS = Object.freeze([
  {
    name: `Contact`,
    link: "/contact",
  },
  /* {
      name: `Code`,
      link: "#",
      subMenu: [
        {
          name: `Computer graphics`,
          link: `/tags/computer-graphics`,
          type: `internal`,
        },
        {
          name: `Computer science`,
          link: `/tags/computer-science`,
          type: `internal`,
        },
      ],
    }, */

  /*{
      name: `(FR) Humanités `,
      link: "/tags/humanities",
    },*/
]);

export const SOCIAL_LINKS = Object.freeze([
  {
    name: `Email`,
    link: `sylvain@artois.io`,
  },
  {
    name: `Twitter`,
    link: `https://twitter.com/sylvainartois`,
  },
]);

//iPhone 8+ is 414 px
export const MQ_IS_MOBILE = "(max-width: 481px)";
