import { merge } from "theme-ui"
import { tailwind } from "@theme-ui/preset-tailwind"
import prism from "@theme-ui/prism/presets/oceanic-next"

export const mainPalette = {
  oxfordBlue: "#000027",
  slateGray: "#757c89",
  newYorkPink: "#cc786b",
  blanchedAlmond: "#ffe8c2",
  platinum: "#e4e1dd",
}

const theme = merge(tailwind, {
  breakpoints: ["480px", "768px", "1024px", "1440px"],
  fonts: {
    siteTitle: '"Alfa Slab One", sans-serif',
    body: '"Libre Baskerville", serif',
    navLinks: '"Quattrocento Sans", sans-serif',
    alt: "inherit",
    heading: '"Alfa Slab One", sans-serif',
    monospace:
      'Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace',
  },
  colors: {
    text: mainPalette.slateGray,
    background: "#ddd",
    primary: mainPalette.newYorkPink,
    secondary: mainPalette.slateGray,
    accent: mainPalette.blanchedAlmond,
    muted: "#cbd5e0",

    ...mainPalette,

    mainPalette,

    header: {
      background: mainPalette.oxfordBlue,
      backgroundOpen: mainPalette.oxfordBlue,
      text: mainPalette.platinum,
      textOpen: mainPalette.platinum,
      icons: mainPalette.platinum,
      iconsHover: mainPalette.blanchedAlmond,
      iconsOpen: mainPalette.platinum,
    },

    footer: {
      background: mainPalette.slateGray,
      text: mainPalette.platinum,
      links: mainPalette.platinum,
      icons: mainPalette.platinum,
    },
  },
  sizes: {
    maxPageWidth: "1440px",
    maxContentWidth: "720px",
  },
  styles: {
    root: {
      ".react-mathjax-preview-result": {
        fontFamily: "navLinks",
      },
    },
    a: {
      color: "primary",
      textDecoration: "none",
      ":hover": {
        color: "accent",
        textDecoration: "none",
      },
    },
    h1: {
      fontWeight: "normal",
      m: 0,
      mb: 1,
      fontSize: 6,
      mt: 4,
    },
    h2: {
      m: 0,
      mb: 1,
      fontSize: 5,
      mt: 4,
    },
    h3: {
      m: 0,
      mb: 1,
      fontSize: 4,
      mt: 3,
    },
    h4: {
      m: 0,
      mb: 1,
      fontSize: 3,
    },
    h5: {
      m: 0,
      mb: 1,
      fontSize: 2,
    },
    h6: {
      m: 0,
      mb: 2,
      fontSize: 1,
    },
    blockquote: {
      backgroundColor: "primary",
      border: "none",
      p: 3,
      my: 3,
      mx: [1, 2, 4, null, null],
      fontFamily: "navLinks",
      color: "white",
      "::after": {
        content: "'\"'",
        fontFamily: "heading",
        fontSize: "100px",
        width: "100%",
        height: "40px",
        opacity: ".3",
        display: "block",
        lineHeight: "100px",
        textAlign: "center",
      },
    },
    inlineCode: {
      color: "text",
      bg: "muted",
      fontSize: 1,
      p: 1,
    },
    pre: {
      ...prism,
      fontSize: 1,
      p: 3,
      overflowX: "scroll",
    },
    table: {
      width: "100%",
      my: 3,
      borderCollapse: "collapse",
    },
    th: {
      verticalAlign: "bottom",
      borderWidth: "2px",
      borderStyle: "solid",
      borderColor: "muted",
      backgroundColor: "muted",
      padding: 2,
      textAlign: "inherit",
    },
    td: {
      borderWidth: "2px",
      borderStyle: "solid",
      borderColor: "muted",
      verticalAlign: "top",
      padding: 2,
    },
  },
  variants: {
    contentContainer: {
      mt: 0,
      mb: 0,
      px: 0,
    },
    header: {
      position: "fixed",
      top: 0,
      width: "100%",
      color: "header.text",
      backgroundColor: "header.background",
      zIndex: "1",
      boxShadow: "0px 2px 3px 0px rgba(0,0,39,1)",
      "::after": {
        position: "absolute",
        top: "100%",
        left: "0",
        width: "100%",
        height: "4px",
        backgroundColor: "primary",
        content: "''",
      },
    },
    footer: {
      color: "footer.text",
      backgroundColor: "footer.background",
      textAlign: "left",
      position: "absolute",
      bottom: "0",
      width: "100vw",
    },
    headerContent: {
      maxWidth: "maxPageWidth",
      width: "100%",
      minHeight: "50px",
      m: "0 auto",
      display: "flex",
      justifyContent: "space-between",
      px: [1, null, 3, null, null],
      py: [1, null, 2, null, null],
    },
    footerContent: {
      maxWidth: "maxPageWidth",
      width: "100%",
      m: "0 auto",
      px: [1, null, 3, null, null],
      py: [1, null, 2, null, null],
    },
    heading: {
      fontFamily: "heading",
      fontSize: "1.875rem",
      color: "primary",
    },
    siteTitle: {
      fontSize: "1.875rem",
      fontWeight: "normal",
      color: mainPalette.platinum,
    },
    navLi: {
      cursor: "pointer",
      fontFamily: "navLinks",
      a: {
        px: 1,
        textDecoration: "none",
        color: "header.text",
        lineHeight: "38px",
        "::after": {
          content: "",
        },
        ":hover, :focus": {
          color: mainPalette.blanchedAlmond,
        },
      },
      ".active": {
        color: mainPalette.blanchedAlmond,
      },
      ".active::after": {
        content: "",
      },
      ul: {
        px: 3,
        a: {
          ":hover": {
            color: mainPalette.blanchedAlmond,
            textDecoration: "none",
          },
        },
      },
    },
    navLinkSubActive: {
      textDecoration: "none",
      color: mainPalette.blanchedAlmond,
    },
    postContainer: {},
    postMeta: {
      color: "black",
      a: {
        color: "black",
      },
    },
    postImage: {
      height: "auto",
      mb: 3,
    },
    srOnly: {
      position: "absolute",
      width: "1px",
      height: "1px",
      padding: 0,
      margin: "-1px",
      overflow: "hidden",
      clip: "rect(0, 0, 0, 0)",
      border: 0,
    },
    navBackdrop: {
      position: "absolute",
      width: "1px",
      height: "1px",
      padding: 0,
      margin: "-1px",
      overflow: "hidden",
      clip: "rect(0, 0, 0, 0)",
      border: 0,
    },
  },
})

export default theme
