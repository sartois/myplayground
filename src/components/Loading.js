/** @jsx jsx */
import { jsx } from "theme-ui"
import { useState, useEffect } from "react"
import { useSpring, animated } from "react-spring"
import { Keyframes } from "react-spring/renderprops"

export default function Loading({ delay = 3, color = "newYorkPink", store }) {
  const [containerVisible, toggle] = useState(true)
  const hideContainerProps = useSpring({
    opacity: containerVisible ? 1 : 0,
  })
  const setReady = store(state => state.setReady)

  useEffect(() => {
    const timer = setTimeout(() => {
      toggle(false)
      setReady(true)
    }, delay * 1000)
    return () => clearTimeout(timer)
  }, [delay, toggle, setReady])

  const Container = Keyframes.Spring(async next => {
    while (true)
      await next({
        opacity: 1,
        transform: "scale(1)",
        from: { opacity: 0, transform: "scale(3)" },
        reset: true,
        config: { duration: 1500 },
      })
  })

  return (
    <animated.div
      style={hideContainerProps}
      sx={{
        position: "absolute",
        width: "100vw",
        height: "100vh",
        top: 0,
        left: 0,
        backgroundColor: color,
      }}
    >
      <Container>
        {styles => (
          <div
            style={styles}
            sx={{
              position: "relative",
              top: "50%",
              left: "50%",
              background: "rgba(255, 255, 255, 1)",
              width: "25px",
              height: "25px",
              marginLeft: "-12px",
              marginTop: "-12px",
              borderRadius: "50%",
            }}
          ></div>
        )}
      </Container>
    </animated.div>
  )
}
