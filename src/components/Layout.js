import React from "react"
import { Helmet } from "react-helmet"
import SiteContainer from "./SiteContainer"
import Header from "./header"
import Main from "./Main"
import ContentContainer from "./ContentContainer"
import DefaultFooter from "./DefaultFooter"

export default function Layout({
  children,
  addHeader = true,
  addFooter = true,
  useMaxContentWidth = true,
}) {
  return (
    <SiteContainer>
      <Helmet>
        <meta charSet="utf-8" />
        <link
          href="https://fonts.googleapis.com/css2?family=Alfa+Slab+One&family=Libre+Baskerville&family=Quattrocento+Sans&display=swap"
          rel="stylesheet"
        />
        <link rel="shortcut icon" href="/favicon/favicon.ico" />
      </Helmet>
      {addHeader && <Header />}
      <Main>
        <ContentContainer useMaxContentWidth={useMaxContentWidth}>
          {children}
        </ContentContainer>
      </Main>
      {addFooter && <DefaultFooter />}
    </SiteContainer>
  )
}
