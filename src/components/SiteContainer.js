/** @jsx jsx */
import { jsx } from "theme-ui";

export default function SiteContainer({ children }) {
  return (
    <div
      sx={{
        variant: "variants.siteContainer",
      }}
    >
      {children}
    </div>
  );
}
