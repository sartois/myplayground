/** @jsx jsx */
import { jsx } from "theme-ui"
import SocialFooter from "./SocialFooter"
import { BLOG_TITLE } from "../constants"

export default function DefaultFooter() {
  return (
    <footer
      sx={{
        a: {
          color: "footer.links",
        },
        variant: "variants.footer",
      }}
    >
      <div
        sx={{
          variant: "variants.footerContent",
        }}
      >
        <div
          sx={{
            ml: 3,
            a: {
              color: "footer.icons",
              mr: 3,
            },
            "a:last-of-type": {
              mr: 0,
            },
            "a:hover": {
              color: "accent",
            },
            float: "left",
          }}
        >
          <SocialFooter />
        </div>
        <p sx={{ m: 0, mr: 1, fontSize: 0, textAlign: "right" }}>
          {BLOG_TITLE} - Sylvain Artois, {new Date().getFullYear()}
        </p>
      </div>
    </footer>
  )
}
