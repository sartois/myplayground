/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import React, { useState } from "react"
import { FiMoreHorizontal, FiChevronRight } from "react-icons/fi"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import SocialFooter from "./SocialFooter"

export default function Hub({ colors, next, card, store }) {
  const { theme } = useThemeUI()
  const [showHub, toggleHub] = useState(true)
  const ready = store(state => state.ready)
  const isPortrait = store(state => state.isPortrait)

  return (
    <>
      {ready && (
        <>
          {false && (
            <Menu
              showHub={showHub}
              toggleHub={toggleHub}
              colors={colors}
              theme={theme}
            />
          )}
          <MainTitle colors={colors} />

          {showHub && card && (
            <div
              sx={{
                position: "absolute",
                top: 0,
                left: 0,
                height: "100vh",
                width: "100%",
              }}
            >
              {card}
            </div>
          )}

          <div sx={{ bottom: [2], left: [3], position: "absolute" }}>
            <SocialFooter
              color={colors.footer || theme.colors.oxfordBlue}
              size={"2rem"}
              style={{
                filter: "drop-shadow(2px 2px 1px rgba(0, 0, 0, 0.37))",
              }}
            />
          </div>
          {next && (
            <div
              sx={{
                top: ["calc(50% - 52px)"],
                right: [2],
                position: "absolute",
                overflow: "hidden",
                background: "rgba(0,0,0,0.5)",
                borderRadius: "100%",
                width: isPortrait ? "64px" : "105px",
                height: isPortrait ? "64px" : "105px",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                pt: isPortrait ? "6px" : "8px",
                pl: isPortrait ? "4px" : "8px",
                transition: "1s background-color ease-out",
                "&:hover": {
                  backgroundColor: "#555555",
                },
              }}
            >
              <AniLink paintDrip to={next} hex={theme.colors.newYorkPink}>
                <FiChevronRight
                  color={colors.nav || theme.colors.oxfordBlue}
                  size={isPortrait ? "4rem" : "6rem"}
                  style={{
                    filter: "drop-shadow(2px 2px 1px rgba(0, 0, 0, 0.37))",
                    transition: "1s color ease-out",
                  }}
                  sx={{
                    "&:hover": {
                      color: `${theme.colors.blanchedAlmond} !important`,
                    },
                  }}
                />
              </AniLink>
            </div>
          )}
        </>
      )}
    </>
  )
}

function MainTitle({ colors }) {
  return (
    <div
      sx={{
        top: [1, 2],
        left: [3],
        position: "absolute",
      }}
    >
      <h1
        sx={{
          color: colors.title,
          fontFamily: "Alfa Slab One",
          fontSize: [4, 6],
          m: 0,
          textShadow: "2px 2px 1px rgba(0, 0, 0, 0.37)",
        }}
      >
        My Playground
      </h1>
      <h2
        sx={{
          color: colors.title,
          fontSize: [2],
          fontFamily: "Alfa Slab One",
          transformOrigin: "left top",
          transform: "rotate(90deg)",
          position: "relative",
          left: "calc(1.25rem + 5px)",
          m: 0,
          textShadow: "2px 2px 1px rgba(0, 0, 0, 0.37)",
          display: "inline-block",
        }}
      >
        Sylvain Artois
      </h2>
    </div>
  )
}

function Menu({ toggleHub, showHub, colors, theme }) {
  return (
    <div sx={{ top: ["50%"], left: [3], position: "absolute" }}>
      <a onClick={() => toggleHub(!showHub)}>
        <div
          sx={{
            transform: showHub ? "rotate(3690deg)" : "none",
            transition: "transform .3s  ease-out",
            transformOrigin: "center center",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <FiMoreHorizontal
            style={{
              filter: "drop-shadow(2px 2px 1px rgba(0, 0, 0, 0.37))",
            }}
            size={"6rem"}
            color={colors.menu || theme.colors.oxfordBlue}
          />
        </div>
      </a>
    </div>
  )
}
