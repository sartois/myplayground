import React from "react"
import { Stats } from "drei"
import Support from "../Support"
import Planes from "./Planes"

export default function LifelinesCanvas({ debug = false, store }) {
  return (
    <Support camera={{ position: [0, 200, 0] }}>
      <ambientLight intensity={0.5} />
      <Planes store={store} />
      {debug && <Stats />}
    </Support>
  )
}
