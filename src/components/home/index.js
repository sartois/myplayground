/** @jsx jsx */
import { jsx } from "theme-ui"
import React, { useEffect, useCallback } from "react"
import create from "zustand"
import LifelinesCanvas from "./LifelinesCanvas"
import FullpageContent from "../FullpageContent"
//import Loading from "../Loading"
import joconde from "../../../content/assets/home/44902576291_cutout.png"
import { mainPalette } from "../../theme/rules"

const pagesColors = {
  deepGrey: "#555555",
  lightGrey: "#a9a9a9",
  gradient: ["#000000", "#555555", "#A9A9A9"],
  title: mainPalette.blanchedAlmond,
  menu: mainPalette.blanchedAlmond,
  footer: mainPalette.blanchedAlmond,
  nav: mainPalette.newYorkPink,
}

const useStore = create(set => ({
  ready: false,
  setReady: isReady => set(() => ({ ready: isReady })),
  isPortrait: false,
  toggleOriention: isPortrait => set(() => ({ isPortrait })),
}))

export default function HomePage() {
  const toggleOriention = useStore(state => state.toggleOriention)

  const matchMediaCb = useCallback(
    mq => {
      toggleOriention(mq.matches)
    },
    [toggleOriention]
  )

  useEffect(() => {
    if (!window) return
    const mq = window.matchMedia("(orientation: portrait)")
    matchMediaCb(mq)
    mq.addListener(matchMediaCb)
    return mq.removeListener(mq)
  }, [matchMediaCb])

  return (
    <FullpageContent colors={pagesColors} next={"/contact"} store={useStore}>
      <Background />
      <LifelinesCanvas store={useStore} />
      <Joconde />
      {/*<Loading store={useStore} />*/}
    </FullpageContent>
  )
}

function Background() {
  const isPortrait = useStore(state => state.isPortrait)
  return (
    <React.Fragment>
      <div
        sx={{
          background: pagesColors.deepGrey,
          position: "absolute",
          left: "62%",
          width: "38%",
          height: "100vh",
        }}
      ></div>
      <div
        sx={{
          position: "absolute",
          top: "25%",
          left: "62%",
          width: "38%",
          height: "75vh",
          overflow: "hidden",
        }}
      >
        <div
          sx={{
            background: pagesColors.lightGrey,
            width: isPortrait ? "150%" : "120%",
            height: "120%",
            position: "relative",
            left: isPortrait ? "-25%" : "-7%",
            top: "35px",
            transform: "rotate(5deg)",
          }}
        ></div>
      </div>
      <div
        sx={{
          position: "absolute",
          background: `linear-gradient(37deg, ${pagesColors.gradient[0]} 0%, ${pagesColors.gradient[1]} 50%, ${pagesColors.gradient[2]} 100%)`,
          width: "62%",
          height: "100vh",
        }}
      ></div>
    </React.Fragment>
  )
}

function Joconde() {
  return (
    <div
      sx={{
        backgroundImage: `url("${joconde}")`,
        backgroundRepeat: "no-repeat",
        backgroundSize: ["120% auto", "75% auto"],
        backgroundPosition: "left bottom",
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
      }}
    ></div>
  )
}
