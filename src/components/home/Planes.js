import React, { useMemo } from "react"
import { useThree } from "react-three-fiber"
import { FlowFieldV3 } from "threeprocgen"
import Lifelines from "./Lifelines"
import { Vector2, Vector3 } from "three"
import { plane1 } from "./plane1.js"

const flowField = new FlowFieldV3(plane1)

flowField.fields = flowField.fields.map(
  field => new Vector3(field.x, field.y, field.z)
)

//how to handle position of plane and responsiveness ?
//how to build flow field in a responsive world ?

export default function Planes({ store }) {
  const isPortrait = store(state => state.isPortrait)
  const planes = useMemo(
    () => [
      {
        name: "plane1",
        start: {
          x: isPortrait ? -150 : -1100,
          y: -800,
          z: isPortrait ? -350 : -500,
        },
        end: {
          x: isPortrait ? 200 : 400,
          y: 0,
          z: isPortrait ? 150 : 1200,
        },
      },
    ],
    [isPortrait]
  )

  return (
    <>
      {planes.map(({ name, ...props }) => (
        <Plane key={name} {...props} />
      ))}
    </>
  )
}

function Plane({ start, end }) {
  const { size } = useThree()
  const lineNumber = useMemo(
    () => Math.min(400, Math.round((size.width * size.height) / 1000)),
    [size]
  )

  const coords = useMemo(() => {
    const widthMin = start.x
    const widthMax = end.x
    const heightMin = start.z
    const heightMax = end.z
    const depthMin = start.y
    const depthMax = end.z
    return {
      widthMin,
      widthMax,
      heightMin,
      heightMax,
      depthMin,
      depthMax,
      width: widthMax + Math.abs(widthMin),
      height: heightMax + Math.abs(heightMin),
    }
  }, [start, end])

  const resolution = useMemo(() => new Vector2(size.width, size.height), [size])

  return (
    <Lifelines
      resolution={resolution}
      lineNumber={lineNumber}
      flowField={flowField}
      coords={coords}
    />
  )
}
