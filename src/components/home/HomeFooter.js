/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import { BLOG_HTML_TITLE, MQ_IS_MOBILE } from "../../constants"

export default function HomeFooter() {
  const { theme } = useThemeUI()
  const fullYear = new Date().getFullYear()

  const mq =
    typeof window !== "undefined"
      ? window.matchMedia(MQ_IS_MOBILE)
      : {
          matches: false,
        }

  return mq.matches ? null : (
    <footer
      sx={{
        color: "footer.text",
        backgroundColor: "footer.background",
        textAlign: "right",
        px: theme.space[2],
        a: {
          color: "footer.links",
        },
        variant: "variants.footer",
        boxShadow: "0px 0px 3px 0px rgba(0,0,39,1);",
        position: "absolute",
        bottom: "5px",
        right: "5px",
      }}
    >
      <span
        sx={{
          m: 0,
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        {BLOG_HTML_TITLE}, {fullYear}
      </span>
      <span
        sx={{
          mx: theme.space[1],
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        -
      </span>
      <span
        sx={{
          m: 0,
          fontSize: theme.fontSizes[0],
          fontFamily: theme.fonts.navLinks,
        }}
      >
        Tribute to Gérard Fromanger
      </span>
    </footer>
  )
}
