import React, { useMemo, useRef, createRef } from "react"
import { useFrame, extend } from "react-three-fiber"
import * as meshline from "threejs-meshline"
import { Follower } from "threeprocgen"
import { Vector3 } from "three"
import { mainPalette } from "../../theme/rules"
import { map, getRandomRangeInt } from "../../lib/mathHelper"

extend(meshline)

const colors = Object.values(mainPalette)

function getInitialParameterForAgent({ widthMin, heightMin, depthMin }) {
  const maxLength = Math.max(10, 100 * Math.random())
  return {
    initialPosition: new Vector3(
      getRandomRangeInt(widthMin - 100, widthMin + 100),
      getRandomRangeInt(depthMin - 100, depthMin + 100),
      getRandomRangeInt(heightMin - 100, heightMin + 100)
    ),
    maxSpeed: maxLength,
    maxForce: maxLength,
  }
}

function getInitialParameterForLine(position) {
  const points = new Array(getRandomRangeInt(10, 100))
    .fill()
    .map(() => position)
  return {
    color: colors[parseInt(colors.length * Math.random())],
    width: Math.max(2, 10 * Math.random()),
    points,
  }
}

export default function Lifelines({
  flowField,
  resolution,
  lineNumber,
  coords,
}) {
  const linesRefs = useRef([])

  let agents = useMemo(
    () =>
      new Array(lineNumber)
        .fill()
        .map(() => new Follower(getInitialParameterForAgent(coords))),
    [coords, lineNumber]
  )

  //build the ref array
  useMemo(() => {
    linesRefs.current = Array(agents.length)
      .fill()
      .map((_, i) => linesRefs.current[i] || createRef())
  }, [agents])

  const linesAttributes = useMemo(
    () =>
      agents.map(agent => getInitialParameterForLine(agent.position.clone())),
    [agents]
  )

  useFrame((_, delta) => {
    agents = agents.map((agent, i) => {
      const s = agent.separate(agents, 100)
      s.multiplyScalar(0.5)
      agent.applyForce(s)

      const ffCoords = getCoordsInFFWorld(agent.position, coords, flowField)
      const ffF = flowField.lookup(ffCoords)
      agent.follow(ffF.set(ffF.x, 0.4, -ffF.y))
      agent.update(delta)

      if (linesRefs.current[i] && linesRefs.current[i].current) {
        if (agent.position.y > 300) {
          agent = new Follower(getInitialParameterForAgent(coords))
          const { points } = getInitialParameterForLine(agent.position.clone())
          linesRefs.current[i].current.setVertices(points)
        } else {
          linesRefs.current[i].current.advance(agent.position.clone())
        }
      } else {
        console.warn(`Missing ref for agent n°${i}`)
      }

      return agent
    })
  })

  return linesAttributes.map(({ points, width, color }, index) => (
    <mesh key={`meshline_${index}`}>
      <meshLine
        attach="geometry"
        vertices={points}
        ref={linesRefs.current[index]}
      />
      <meshLineMaterial
        attach="material"
        lineWidth={width}
        color={color}
        resolution={resolution}
        sizeAttenuation={0}
      />
    </mesh>
  ))
}

function getCoordsInFFWorld(
  { x, y, z },
  { widthMin, widthMax, heightMin, heightMax },
  { width: ffWidth, height: ffHeight }
) {
  return {
    x: map(x, widthMin, widthMax, 0, ffWidth, true),
    y: map(z, heightMin, heightMax, 0, ffHeight, true),
  }
}
