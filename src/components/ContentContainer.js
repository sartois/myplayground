/** @jsx jsx */
import { jsx } from "theme-ui"

export default function ContentContainer({ children, useMaxContentWidth }) {
  return (
    <div
      sx={{
        maxWidth: useMaxContentWidth ? "maxContentWidth" : "auto",
        my: 0,
        mx: "auto",
        px: 3,
        variant: "variants.contentContainer",
      }}
    >
      {children}
    </div>
  )
}
