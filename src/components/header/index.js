/** @jsx jsx */
import { jsx } from "theme-ui"
import { Global } from "@emotion/core"
import SiteTitle from "./SiteTitle"
import Nav from "./Nav"
import Hamburger from "./MobileButton"

export default function Header() {
  return (
    <header
      sx={{
        variant: "variants.header",
      }}
    >
      <div
        sx={{
          variant: "variants.headerContent",
        }}
      >
        <Global
          styles={theme => ({
            ".main-menu:target + .backdrop": {
              position: "absolute",
              display: "block",
              left: "0",
              top: "0",
              width: "100vw",
              height: "100vh",
              zIndex: "998",
              background: theme.colors.oxfordBlue,
              cursor: "default",
            },
            ".srOnly": {
              position: "absolute",
              width: "1px",
              height: "1px",
              padding: 0,
              margin: "-1px",
              overflow: "hidden",
              clip: "rect(0, 0, 0, 0)",
              border: 0,
            },
          })}
        />
        <SiteTitle />
        <Nav />
        <a href="#main-menu-toggle" className="backdrop" hidden>
          &nbsp;
        </a>
        <Hamburger />
      </div>
    </header>
  )
}
