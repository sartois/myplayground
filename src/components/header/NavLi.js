/** @jsx jsx */
import { jsx } from "theme-ui";

export default function NavLi({ children }) {
  return (
    <li
      sx={{
        variant: "variants.navLi",
      }}
    >
      {children}
    </li>
  );
}
