/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import { FaBars, FaRegWindowClose } from "react-icons/fa"

export function CloseMobileSidebarBtn() {
  const { theme } = useThemeUI()

  return (
    <a
      href="#main-menu-toggle"
      id="main-menu-close"
      aria-label="Close main menu"
      sx={{ display: ["block", "none"] }}
    >
      <span className="srOnly">Close main menu</span>
      <FaRegWindowClose color={theme.colors.platinum} />
    </a>
  )
}

export default function Hamburger() {
  const { theme } = useThemeUI()
  return (
    <a
      id="main-menu-toggle"
      href="#main-menu"
      aria-label="Open main menu"
      sx={{
        display: ["inline-block", "none"],
        padding: ".75em",
        lineHeight: "1em",
        fontSize: "1em",
      }}
    >
      <FaBars color={theme.colors.platinum} />
    </a>
  )
}
