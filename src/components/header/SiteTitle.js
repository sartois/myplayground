/** @jsx jsx */
import { jsx, Styled } from "theme-ui"
import { Link } from "gatsby"
import { BLOG_TITLE } from "../../constants"

export default function SiteTitle() {
  return (
    <Styled.h1
      as="span"
      sx={{
        flex: "0 0 auto",
        m: 0,
        variant: "variants.siteTitle",
        cursor: "pointer",
        a: {
          color: "inherit",
        },
      }}
    >
      <Link to="/">{BLOG_TITLE}</Link>
    </Styled.h1>
  )
}
