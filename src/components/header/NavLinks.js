import React from "react"
/** @jsx jsx */
import { jsx } from "theme-ui"
import NavUL from "./NavUl"
import NavLI from "./NavLi"
import NavUlDropdown from "./NavUlDropdown"
import NavLiDropdown from "./NavLiDropdown"
import NavMenuLinkInternal from "./NavMenuLinkInternal"
import NavMenuLink from "./NavMenuLink"
import { MENU_LINKS } from "../../constants"

export default function NavLinks() {
  return (
    <React.Fragment>
      <h2
        id="main-menu-heading"
        sx={{
          variant: "variants.srOnly",
        }}
      >
        Main menu
      </h2>

      <NavUL>
        {MENU_LINKS.map(({ name, link, subMenu }) => (
          <NavLI key={name}>
            <>
              <NavMenuLinkInternal
                link={link}
                hasSubmenu={subMenu && subMenu.length > 0}
              >
                {name}
              </NavMenuLinkInternal>
              {subMenu && subMenu.length > 0 && (
                <NavUlDropdown>
                  {subMenu.map(subLink => (
                    <NavLiDropdown key={subLink.name}>
                      <NavMenuLink link={subLink.link}>
                        {subLink.name}
                      </NavMenuLink>
                    </NavLiDropdown>
                  ))}
                </NavUlDropdown>
              )}
            </>
          </NavLI>
        ))}
      </NavUL>
    </React.Fragment>
  )
}
