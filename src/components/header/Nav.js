/** @jsx jsx */
import { jsx } from "theme-ui"
import NavLinks from "./NavLinks"
import { CloseMobileSidebarBtn } from "./MobileButton"

export default function Nav() {
  return (
    <nav
      id="main-menu"
      className="main-menu"
      sx={{
        position: ["absolute", "relative"],
        display: ["none", "block"],
        left: ["-200px", "auto"],
        top: ["0", "auto"],
        height: ["100%", "auto"],
        overflowY: "scroll",
        overflowX: "visible",
        transition: "left 0.3s ease, box-shadow 0.3s ease",
        zIndex: 999,
        ":target": {
          display: "block",
          left: "0",
          outline: "none",
        },
        ":target #main-menu-close": {
          zIndex: 1001,
        },
        ":target ul": {
          position: "relative",
          zIndex: 1000,
        },
      }}
      aria-label="Primary menu"
    >
      <CloseMobileSidebarBtn />
      <NavLinks />
    </nav>
  )
}
