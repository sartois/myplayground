import React from "react"
/** @jsx jsx */
import { jsx } from "theme-ui"
import { Link } from "gatsby"

export default function NavMenuLinkInternal({ link, hasSubmenu, children }) {
  return (
    <Link to={link} activeClassName="active" aria-haspopup={hasSubmenu}>
      <React.Fragment>
        {hasSubmenu && <span sx={{ fontSize: "80%" }}> {"\u25BF"}</span>}
        {children}
      </React.Fragment>
    </Link>
  )
}
