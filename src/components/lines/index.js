/** @jsx jsx */
import { jsx, useThemeUI } from "theme-ui"
import AnimatedBkg from "./AnimatedBkg"
import FullpageContent from "../FullpageContent"
import Support from "../Support"

const noop = () => {}

export default function HomePage() {
  const { theme } = useThemeUI()
  return (
    <FullpageContent store={noop} colors={{}}>
      <Support camera={{ position: [0, 0, 500] }} orthographic>
        <AnimatedBkg theme={theme} />
      </Support>
    </FullpageContent>
  )
}
