import React, { useMemo } from "react"
import { useLoader, useThree } from "react-three-fiber"
import { TextureLoader, LinearFilter, Vector2 } from "three"
import { BoidsTrail } from "./BoidsTrail"
import joconde from "../../../content/assets/home/44902576291_cutout(1).png"

export default function AnimatedBkg({ theme }) {
  const texture = useLoader(TextureLoader, joconde)
  const { aspect, scene } = useThree()

  useMemo(() => {
    //hardcoded for joconde
    const texAspect = 2.16998
    const relAspect = aspect / texAspect

    texture.minFilter = LinearFilter

    //source https://code-examples.net/en/q/12f1fc1
    texture.repeat = new Vector2(
      Math.max(relAspect, 1),
      Math.max(1 / relAspect, 1)
    )

    //@todo a bit small on mobile

    scene.background = texture
  }, [texture, aspect, scene])

  return <BoidsTrail theme={theme} />
}
