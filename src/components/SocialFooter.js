/**@jsx jsx */
import { jsx } from "theme-ui"
import { FaRegEnvelope, FaTwitter } from "react-icons/fa"
import { SOCIAL_LINKS } from "../constants"

export default function SocialFooter(props) {
  return SOCIAL_LINKS.map(({ name, link }) => {
    let socialName = name.toLowerCase()
    if (socialName === "twitter") {
      return (
        <a
          href={link}
          aria-label={name}
          target="_blank"
          rel="noopener noreferrer"
          key={name}
          sx={{ marginLeft: [4] }}
        >
          <FaTwitter {...props} />
        </a>
      )
    }
    if (socialName === "email") {
      let email = "mailto:" + link
      return (
        <a href={email} aria-label="Email" rel="noopener noreferrer" key={name}>
          <FaRegEnvelope {...props} />
        </a>
      )
    }
    return null
  })
}
