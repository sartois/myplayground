/** @jsx jsx */
import { jsx } from "theme-ui"
//import Hub from "./Hub"

export default function FullpageContent({ children, ...props }) {
  return (
    <div sx={{ width: "100%", height: "100vh", position: "relative" }}>
      {children}
      {/*<Hub {...props} />*/}
    </div>
  )
}
