import React, { Suspense } from "react"
import { Canvas } from "react-three-fiber"

export default function Support({ children, fallback, ...props }) {
  const devicePixelRatio =
    typeof window !== "undefined" ? window.devicePixelRatio : 1

  return (
    <Canvas concurrent pixelRatio={devicePixelRatio} {...props}>
      <Suspense fallback={fallback || null}>{children}</Suspense>
    </Canvas>
  )
}
