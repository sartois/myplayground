import React from "react"
import { Stats } from "drei"
import Support from "../Support"
import ContactAnimatedBkg from "./ContactAnimatedBkg"

export default function ContactCanvas({ debug = false, store }) {
  return (
    <Support camera={{ position: [0, 0, 100] }}>
      <ambientLight intensity={0.5} />
      <ContactAnimatedBkg store={store} />
      {debug && <Stats />}
      {debug && <axesHelper args={[20]} />}
    </Support>
  )
}
