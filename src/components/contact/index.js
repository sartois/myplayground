/** @jsx jsx */
import { jsx } from "theme-ui"
import React from "react"
import create from "zustand"
import Loading from "../Loading"
import { mainPalette } from "../../theme/rules"
import FullpageContent from "../FullpageContent"
import ContactCanvas from "./ContactCanvas"

const pagesColors = {
  background: mainPalette.oxfordBlue,
  title: mainPalette.blanchedAlmond,
  menu: mainPalette.blanchedAlmond,
  footer: mainPalette.blanchedAlmond,
  nav: mainPalette.newYorkPink,
  card: mainPalette.platinum,
}

const useStore = create(set => ({
  ready: false,
  setReady: isReady => set(() => ({ ready: isReady })),
}))

export default function ContactPage() {
  return (
    <FullpageContent colors={pagesColors} store={useStore}>
      <Background />
      <ContactCanvas store={useStore} />
      <Loading store={useStore} />
    </FullpageContent>
  )
}

function Card() {
  return (
    <section
      sx={{
        position: "absolute",
        top: "10%",
        left: "10%",
        width: "300px",
        height: "100px",
        backgroundColor: "rgba(255, 232, 194, 0.5)",
        borderRadius: "5px",
        boxShadow: "2px 2px 1px rgba(0, 0, 0, 0.37)",
      }}
    >
      CARD
    </section>
  )
}

function Background() {
  return (
    <React.Fragment>
      <div
        sx={{
          background: pagesColors.background,
          position: "absolute",
          left: 0,
          top: 0,
          width: "100%",
          height: "100vh",
        }}
      ></div>
    </React.Fragment>
  )
}
