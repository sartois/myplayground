import React, { useMemo, useRef, useCallback } from "react"
import { useLoader, useFrame, useThree } from "react-three-fiber"
import { TextureLoader } from "three"
import getImageParticuleGeometry from "../../lib/imageParticule/getImageParticuleGeometry"
import "../../lib/imageParticule/ImageParticuleMaterial"
import parisMetro from "../../../content/assets/contact/IMGP0893_scaleddown.jpg"

export default function ContactAnimatedBkg() {
  const texture = useLoader(TextureLoader, parisMetro)
  const photoMaterial = useRef()
  const { size, viewport } = useThree()

  const [geometry, textureSize] = useMemo(() => {
    return getImageParticuleGeometry(texture, size)
  }, [texture, size])

  useFrame(({ clock }) => {
    if (photoMaterial.current) {
      photoMaterial.current.time = clock.getElapsedTime()
    }
  })

  const position = useMemo(() => {
    return {
      x: Math.round(textureSize.width / -2),
      y: Math.round(textureSize.height / -2),
    }
  }, [textureSize])

  const particulesMesh = useCallback(mesh => {
    if (mesh !== null) {
      mesh.material.size = 2
      mesh.material.random = 1.0
      mesh.material.depth = 4.0
    }
  }, [])

  return (
    <>
      <mesh
        geometry={geometry}
        ref={particulesMesh}
        position={[position.x, position.y, 0]}
        frustumCulled={false}
      >
        <imageParticuleMaterial
          attach="material"
          textureSize={textureSize}
          texture={texture}
          ref={photoMaterial}
        />
      </mesh>
    </>
  )
}
