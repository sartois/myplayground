/** @jsx jsx */
import { jsx } from "theme-ui";

export default function SiteMain({ children }) {
  return (
    <main
      sx={{
        variant: "variants.main",
      }}
    >
      {children}
    </main>
  );
}
