import React from "react"
import { Helmet } from "react-helmet"
import Layout from "../components/Layout"
import ContactPage from "../components/contact"
import { BLOG_HTML_TITLE } from "../constants"

export default function Home() {
  return (
    <Layout addFooter={false} addHeader={false} useMaxContentWidth={false}>
      <Helmet>
        <title>
          {BLOG_HTML_TITLE}
          {" - Contact"}
        </title>
      </Helmet>
      <ContactPage />
    </Layout>
  )
}
